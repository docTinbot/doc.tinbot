.. _configurandoWebhook:
Configurando Webhook
====================

Uma opção boa para usar com o Robô é a utilização de Webhook para uso externo e integração com outros sistemas. Dando várias possibilidades para envio de comandos ao Robô.

Para criar um webhook é necessário ter permissão de administrador do time que contém o Robô. Caso tenha permissão, vá em Gerenciamento > Hooks.
Selecione um time e clique em adicionar.

.. warning::

	Caso não tenha permissão de administrador contate o resposável pelo Robô e solicite o acesso para criação do hook ou o hook já criado.


.. figure:: .attachments/tela-hook.png

Como mostrado na imagem acima, será necessário selecionar um time para qual deseja gerar o Hook. Lembrando que pode ser gerado um Robô em específico ou para os times.
Ao clicar em "Adicionar", abrirá uma tela onde será necessário dar um nome para esse novo hook. Após isso é só salvar e o acesso estará pronto.

.. figure:: .attachments/criando-hook.png

Você poderá criar quantos hooks forem necessários e excluí-los quando for preciso. Lembre-se que ao excluir o acesso por aquele link não ficará mais disponível.

Após ter salvo, a criação do hook, será exibido na listagem uma URL que iremos usar para comunicar com o Robô.

.. figure:: .attachments/urls-webhook.png

Uso da URL gerada no hook
-------------------------

Com a URL gerada, podemos utilizá-la para fazer integrações com outros sistemas que não necessitem de acesso ao portal ou autenticação.
Dessa forma temos a opção de **enviar mensagem** ou **chamar uma ação**. Abaixo como podemos usar a URL gerada.

Um exemplo de uso para o Hook é através do :ref:`Controle Remoto <ControleRemoto>`, onde podemos utilizá-lo para enviar comandos ao Robô instantaneamente sem a necessidade de interação ou acessar o portal.
Há outros exemplos que podemos citar, um deles seria a utilização do Hook para avisar quando ações de uma determinada empresa tivessem uma queda considerável. Ou, até mesmo, avisar uma equipe de desenvolvimento que houve novas atualizações no software.

.. note::

	Após gerada uma URL, devemos nos atentar se o Hook foi gerado para o Robô ou para algum time pois isso deve ser considerado na hora do uso da URL.
	
Enviando mensagem
~~~~~~~~~~~~~~~~
Para enviar uma mensagem diretamente ao Robô, é necessário passar como parâmetro ``ticotico`` com a mensagem que deseja

``GET https://api.tinbot.club/hooks/302f7e8e-7137-486a-99d3-522f6f58fd08?ticotico=Olá pessoas``

Enviando mensagem e passando o nome do robô
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Para enviar uma mensagem onde o Hook criado foi para um time, que não seja o robô, é necessário passar como parâmetro ``robotname`` e ``ticotico`` com a mensagem.

``GET https://api.tinbot.club/hooks/302f7e8e-7137-486a-99d3-522f6f58fd08?robotname=tinbot1000&ticotico=Olá galera``

Enviando comando para executar uma ação
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Para enviar uma mensagem diretamente ao Robô solicitando a execução de uma ação, é necessário passar como parâmetro ``actioncode`` com o código da ação

``GET https://api.tinbot.club/hooks/302f7e8e-7137-486a-99d3-522f6f58fd08?actioncode=123``

Enviando comando para executar uma ação e passando o nome do robô
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Para enviar uma mensagem onde o Hook criado foi para um time, que não seja o robô, é necessário passar como parâmetro ``robotname`` e ``actioncode`` com o código da ação.

``GET https://api.tinbot.club/hooks/302f7e8e-7137-486a-99d3-522f6f58fd08?robotname=tinbot1000&actioncode=123``

Enviando comando passando parâmetros para a ação
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Para enviar um comando que seja necessário passar parâmetros será preciso enviá-los pelo corpo da requisição. 

``POST https://api.tinbot.club/hooks/302f7e8e-7137-486a-99d3-522f6f58fd08?robotname=tinbot1000&actioncode=123``

Com o corpo da requisição sendo:

.. code-block:: json

	{
		"parametro" : "valor"
	}
	
.. code-block:: curl

	curl -X POST \
	  'https://api.tinbot.club/hooks/65fd219a-c113-40da-9e68-1ddc88c6137d?robotname=tinbot1012&actioncode=226' \
	  -H 'cache-control: no-cache' \
	  -H 'content-type: application/json' \
	  -d '{
		"name": "João"
	}
	'

