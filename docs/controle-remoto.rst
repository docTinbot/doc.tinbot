.. _ControleRemoto:
Controle remoto
===============

A tela de Controle Remoto tem como objetivo facilitar o uso, podendo ser enviado comandos (mensagens) que não estão cadastrados no Tinbot aumentando as possibilidades de interação por parte das pessoas que estão próximas a ele.


O uso
-----

Antes de começar a enviar comandos ao Tinbot, é necessário ter uma URL que dará acesso e encaminhará os comandos ao Tinbot. Essa URL é chamada de :ref:`Hook <configurandoWebhook>`.
Caso possua esse :ref:`Hook <configurandoWebhook>`, insira ela no campo URL. Caso não possua, será necessário :ref:`criar <configurandoWebhook>` uma ou solicitar ao resposável pelo Portal Tinbot.

.. figure:: .attachments/remote-control-url.png

Após inserido o Hook no campo URL, há duas opções para começar a enviar comandos ao Tinbot. Uma delas é usar os comandos já existentes (podendo editá-los) ou criar novos.
Caso queira criar, basta clicar em '+' e adicionar o comando no campo em branco.

.. figure:: .attachments/remote-control-mobile.png

O próximo passo é executar os comandos que deseja, para isso é só clicar no 'Play'. Caso o Hook esteja correto e o Tinbot online, o comando será enviado com sucesso.
Caso contrário será exibida uma mensagem com o motivo de não ter sucesso.


Há outras opções para o uso da tela, são elas:

.. figure:: .attachments/remote-control-upload-donwload.png

- Exportar mensagens (Donwload mensagens), gerando um arquivo texto(TXT)
    Essa opção tem como objetivo o compartilhamento de mensagens entre pessoas a fim de agilizar o uso. 
    Dessa forma, ao Exportar será gerado um arquivo de texto com todas as mensagem disponíveis na tela.

- Importar mensagens (Upload mensagens), a partir de um arquivo de texto(TXT)
    Essa opção tem como objetivo utilizar mensagens a partir de um arquivo de texto exportado anteriormente.

.. figure:: .attachments/remote-control-options.png

- Duplicar
    É duplicado a linha em especifico, facilitanto o uso do mesmo texto e assim podendo alterar sem perder a informação do comando original.

- Limpar
    Limpa a linha, apagando toda informação contida nela.

- Excluir
    Exclui a linha da listagem


Observação
----------

Todos os registros criados/alterados junto com o Hook é salvo no navegador, podendo fechar/recarregar a tela sem perda de dados. 
