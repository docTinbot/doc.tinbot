.. _Triggers:
Triggers
========

Objetivo
--------

Criar eventos responsáveis por disparar ações no Robô. Podendo ser do tipo **Voz**, **Agendamento** ou **Pooling (JS)**.

Funcionalidades
---------------

Na listagem de Triggers é possível visualizar todas cadastradas para o time selecionado e paraos times superiores ao selecionado. Além disso, pode-se inserir, excluir e editar as triggers do time. No caso de triggers de times superiores, só é permitido ativar/desativar a mesma.

.. figure:: .attachments/tela-triggers.png

Para **seleção de time** um dropdown lista todos os Times que o usuário logado tem Acesso

.. figure:: .attachments/image-e20c9fde-1a2f-4614-9e20-0e4fb62a9d3f.png

Para **listagem de triggers** são listados todas as Triggers cadastradas para o time e seus superiores. No caso de triggers de superiores, a cor será diferente. E a cor se diferencia quando a trigger estiver ativa ou desativada no time de origem.

Quando azul mais claro, trigger desativada no time de origem

.. figure:: .attachments/trigger-desativada-pelo-time-origem.png

Quando azul mais escuro, trigger ativa no time de origem

.. figure:: .attachments/trigger-ativa-no-time-de-origem.png

Dessa forma a listagem completa ficará assim:

.. figure:: .attachments/listagem-trigger.png

Adição de nova trigger
----------------------

Para adicionar uma nova trigger é necessário selecionar primeiramente um tipo de trigger. Após seleção o botão 'Adicionar' ficará disponível para ser clicado

.. figure:: .attachments/botao-adicionar-e-tipos-triggers.png
.. figure:: .attachments/botao-adicionar-ativo-e-tipos-triggers.png

	
Tipos de trigger
~~~~~~~~~~~~~~~~

.. _voz:
Voz
***
Esse tipo consiste numa trigger simples onde é passado uma condição e selecionado uma ação para a mesma. Ou seja, quando esse tipo exister e for chamada no robô, irá responder com a ação selecionada.

.. figure:: .attachments/trigger-voz.png

JSPooling
*********
Trigger codificada com código javascript executado de tempos em tempos que avalia uma condição qualquer e, quando satisfeita a condição, dispara a ação correspondente.
O campo "Tempo mínimo entre execuções" tem como objetivo definir de quanto em quanto tempo será executado a ação selecionada, o tempo mínimo e máximo é entre 1 e 24 horas.
O campo "Intervalo de teste da condição" tem como objetivo definir de quanto em quanto tempo será executado teste para verificar se a ação já foi executada, obedecendo o tempo entre as excecuções. O tempo mínimo e maximo entre cada teste é de 5 e 9999 minutos.

.. figure:: .attachments/trigger-jspooling.png

Agendamento
***********
Esse tipo consiste em disparar uma ação no robô no período agendado. Podendo atribuir uma ação à um horário do dia, à um dia da semana ou com repetição entre determinados dias.
Todos agendamentos podem ter data de início e fim para a execução, respeitando o horário e dia selecionado.

.. figure:: .attachments/trigger-agendamento.PNG

Imprimir
--------

O botão imprimir tem a finalidade de exibir em formato PDF todas as Triggers disponíveis no time acessado, podendo se basear nos filtros selecionados.

.. figure:: .attachments/botao-imprimir-trigger.png

.. figure:: .attachments/listagem-imprimir-trigger.png