O reset se dará por meio da opção "Esqueci a senha" na tela de login.

![image.png](/.attachments/image-b62d06b2-db48-493f-84e7-69c4c099a70a.png)

Após o mesmo ser clicado será aberto um modal com a função de recuperar senha.

![image.png](/.attachments/image-bac8a730-c59b-4612-a64b-05d0c528c6eb.png)

Após isso, o sistema irá resetar a senha gerando um token tempórario que será enviado por email para o mesmo poder acessar e inserir uma nova senha.

O envio e gerenciamento do envio será feito por meio da ferramenta SendGrid. Tal componente está disponivel para ser usado tando em sistemas .net como .net core. Para que tal funcionalidade funcione, é necessário o cadastro no sistema [SendGrid](https://sendgrid.com) com o email a ser utilizado.

([link](https://app.sendgrid.com/) para o app SendGrid)

Quando clicado no link enviado por email , será redirecionado a tela de troca de senha.

![image.png](/.attachments/image-e3bab70f-e8a6-4237-8ab8-53694ad8c1ad.png)