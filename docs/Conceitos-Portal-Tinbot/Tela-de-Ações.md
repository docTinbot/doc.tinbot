####Objetivo
Listar as ações para o Tinbot selecionado, permitir a exclusão de ações, dar acesso as telas de inserção e de edição de ações.

![image.png](/.attachments/image-ba2fae9d-35f9-4675-bbdf-ae91492f6f60.png)

####Funcionalidades

- Seleção do Tinbot - um dropdown lista todos os Times que o usuário logado tem Acesso
![image.png](/.attachments/image-5efcdbf0-75d2-4b06-8a9a-71c5aa912506.png) 
![image.png](/.attachments/image-e20c9fde-1a2f-4614-9e20-0e4fb62a9d3f.png)

- Listagem de ações - são listadas todas as ações cadastrasdas no Time selecionado
![image.png](/.attachments/image-cb9e7ef5-1279-4c96-b993-cef1d41bf929.png)

- Adição de TicoTico - direciona para a tela de inclusão de uma nova ação do tipo TicoTico
Botão ativo somente após seleção de Tinbot
![image.png](/.attachments/image-cf5bce96-8652-4447-847b-3c1569de1558.png)

- Adição de Javascript - direciona para a tela de inclusão de uma nova ação do tipo Javascript
Botão ativo somente após seleção de Tinbot
![image.png](/.attachments/image-139bf594-8b7e-4ec9-b0f4-458b9a75d361.png)

- Edição de Ação - direciona pata a tela de edição do tipo de ação correspondente
![image.png](/.attachments/image-70efba64-556c-4345-a202-ed27d83606b8.png)

- Exclusão de ação - exlui a ação selecionada
![image.png](/.attachments/image-7d6e3cfc-14de-4c93-89fc-ff79d8702879.png)
![image.png](/.attachments/image-febf128d-1bb7-4ea5-a4de-8f5fdf719521.png)