####Objetivo
Criar, editar ou executar uma ação no Tinbot através do TicoTico
![image.png](/.attachments/image-78c7b5bc-15f0-4755-9f45-be156d91e9b3.png)

####Funcionalidade
* Edição de código com editor "Moncao Editor" ou smilar	
![image.png](/.attachments/image-08906218-58f8-454f-9696-4efaa7e717d2.png)

* lista com os comandos suportados	
![image.png](/.attachments/image-44d5caca-889a-4422-a413-440b5aff463e.png)

* Listagem de Times Folha para execução do código	
	** Serão listados apenas os Times Folha que estão "online" e que o usuário tem Acesso
	** O Botão "play" executa o código no Folha selecionado
![image.png](/.attachments/image-33f1d248-e540-40c9-a240-7812356cdf58.png)