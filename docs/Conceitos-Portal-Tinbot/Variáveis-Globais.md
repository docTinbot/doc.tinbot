####Objetivo
Criar, editar e excluir variáveis criadas para um determinado time que posteriormente venha ser utilizado dentro do robô.

![image.png](/.attachments/image-20cdc6a8-c562-4e04-9fab-8ffb067d7631.png)

Toda edição, adição e exclusão é feita na tela de listagem.

Um exemplo de uso de tais variáveis é em uma ação criada esperando um parametro de entrada. Nesse caso, na tela de Triggers caso não seja cadastrado os parâmetros dessa ação, será utilizado o parâmetro cadastrado para o mesmo time aqui nas variáveis globais.