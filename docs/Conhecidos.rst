Conhecidos
==========

Objetivo
--------

Tem por finalidade centralizar e cadastrar pessoas/fotos para, por fim, possuir uma base de identificação de pessoas por parte do Robô. Sendo assim, com ação e trigger cadastrado para utilizar tais fotos, ao executar o comando o Robô será capaz de identificar e conversar com a pessoa.

Funcionalidade
--------------

Listagem
********

A listagem é responsável por exibir pessoas cadastradas para os times. 
Na tela é possível ver o status do time, status das pessoas, editar e excluir pessoas.

- Status Time: Campo visual com o objetivo de informar se as fotos das pessoas para o time estão treinadas pelo aplicativo responsável pelo recohecimento das faces. Há três status para o time, que são:
    - Treinado: fotos das pessoas para o time foram treinadas com sucesso e estão prontos para serem utilizadas para reconhecimento facial
    - Aguardando Treinamento: há atualizações em pessoas e o time está esperando para ser treinado 
    - Erro: Pode ser problemas relacionado com a internet na hora do treinamento ou o time ou pessoas estão com algum erro que impossibilitou o treinamento. Ele tentará executar o treinamento novamente em breve

- Status da Pessoa: Campo visual com objetivo de informar se as fotos cadastrada para determinado time estão validadas ou não. Os status podem ser:
    - Ok: Quando as fotos foram validadas e estão todas corretas
    - Aguarando validação: quando ainda não foram validadas
    - Erro: Quando uma ou mais fotos apresentam problemas que podem ser desde fotos de baixa qualidade, que não possuem rotos ou ainda que há mais de uma pessoa


.. figure:: .attachments/listagem-conhecidos.png


Cadastro/Edição
***************
Na tela de criar/editar deve ser inserido as **Informações Gerais** e as **Fotos**. 

Além disso, há os campos de **Informações complementares** que podem ser usados para detalhar o conhecido que está sendo inserido. Tais informações podem ajudar o Tinbot para informações mais especificas que exigem mais do que nome e senha. Um exemplo seria a informação de UserName para filtrar indicadores e informar ao conhecido sobre seu desempenho. 

As fotos devem ser sempre de uma pessoa, preferencialmente do rosto. Tais fotos servem para o rastreio de face feito pelo Robô e posteriormente identificação das pessoas. Quanto mais fotos, nítidas, do rosto em ângulos diferentes, melhor será a identificação.

Cada foto, após o cadastro, será validada e poderá ser rejeitada e ficar marcada com alguma mensagem. Isso pode acontecer caso não tenha foto de rosto, tenha mais de uma pessoa ou a mesma esteja com a qualidade muito baixa.

.. figure:: .attachments/cadastro-conhecidos.png


Utilizando reconhecimento Facial no Robô
----------------------------------------

Após o cadastro das pessoas e suas respectivas fotos, podemos verificar com o Robô se ele consegue reconhecer as pessoas.
Para isso o que será necessário é esperar o treinamento das fotos inseridas.

Caso tudo esteja certo, devemos possuir uma Action do tipo javascript que fará o uso do reconhecimento facial.
Segue exemplo abaixo:

.. figure:: .attachments/codigo-action-reconhecimento-facial.png

Veja que há "recon(7)" na imagem, o "recon" será o responsável por solicitar o reconhecimento facial. O "7" significa o código do time para qual será feito o reconhecimento.

Após cadastrar a ação de reconhecimento, será necessário cadastrar uma Trigger para o Robô que será responsável por 'chamar' a ação cadastrada anteriormente.

Segue o exemplo:

.. figure:: .attachments/cadastro-trigger-reconhecimento-facial.png

Ao finalizar o cadastro da Trigger, com o Robô por perto e ele sinalizando que houve atualização de Triggers, se posicione na frente dele e chame a condição "Quem sou eu". Ao fazer isso, o Robõ fará o reconhecimento facial
e caso ele encontre alguma foto semelhante no cadastro de Conhecidos ele irá falar o nome da pessoa que está vendo.
