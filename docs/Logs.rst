Logs
====

Objetivo
--------

Listar logs do Robô para acompanhamento e identificação de possíveis erros. Todos os logs apresentado nessa tela são enviados do Robô.
Essa tela tem um perfil mais técnico, podendo ser usado para resolver problemas sem a necessidade da presença de um técnico. 
Facilitando e diminuindo a espera na solução de problemas.

.. figure:: .attachments/listagem-logs.png

Funcionalidade
--------------

Para que os logs sejam exibidos é necessário que o Robô esteja online. Dessa forma, basta selecionar o Robô na listagem e deverá começar a aparecer os logs em tempo real do mesmo.

.. figure:: .attachments/selecao-tinbot.png