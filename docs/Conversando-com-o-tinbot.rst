Conversando com o Tinbot
------------------------

Após criar uma ação e uma trigger, chegou a hora de interagir com o robô.

Como é a nossa primeira interação e o robô ainda está com as configurações de fábrica, iremos utilizar o ativador "Hello Tinbot" para comunicar com ele.

E o que seria um Ativador? Ativador é uma palavra ou frase que quando pronunciada o Robô entenderá que você está querendo comunicar com ele. 
Com isso ele irá ativar o microfone para ouvir o que você deseja e assim executar uma ação, caso a mesma esteja cadastrada.

Quando falarmos "Hello Tinbot" ele irá emitir um sinal "bip" e após isso é só falar "Boa tarde". 
O Robô irá interpretar o que foi dito e irá procurar algo que se encaixe nessa fala. Como cadastramos anteiormente uma trigger "Boa tarde", ele irá executar a ação vinculada a trigger
e responderá "Boa tarde humano".

Toda vez que você se comunicar com o Robô ele deverá interpretar o que foi dito e responder com uma ação vinculada a essa condição.

Bom uso :)